pragma foreign_keys=on;

-- CLEAR AND BUILD
.read util/reset.sql

-- BUILD
.print "CREATING TABLE- DataPackageTypes"
.read tables/DataPackageTypes.sql
.print "CREATING TABLE- Orders"
.read tables/Orders.sql

-- SEED
.print "SEEDING DATA - DataPackageTypes"
.read seed/DataPackageTypes.sql
.print "SEEDING DATA - Orders"
.read seed/Orders.sql
