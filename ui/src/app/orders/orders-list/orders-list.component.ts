import { Component, OnInit, ViewChild } from '@angular/core';
import { ApiService } from '@app/api.service';
import { Order } from '../models';
import { Observable, of, throwError, EMPTY } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { MatSnackBar, MatTable } from '@angular/material';
import { Router } from '@angular/router';

@Component({
  selector: 'app-orders-list',
  templateUrl: './orders-list.component.html',
  styleUrls: ['./orders-list.component.scss']
})
export class OrdersListComponent implements OnInit {

  displayedColumns: string[] = ['Name', 'MaxBidPrice', 'DataPackageType', 'Action'];
  orders: Order[];
  loading:boolean = true;
  @ViewChild(MatTable) matTable:MatTable<Order[]>;
  constructor(
    readonly api: ApiService,
    readonly snackbar: MatSnackBar,
    readonly router: Router
  ) { }

  ngOnInit() {
    this.api.getOrders().pipe(
       map(orders => {
          this.loading = false;
          return orders;
        }),
        catchError(() => {
          this.loading = false;
          return of([]);
        })
      ).subscribe(orders => this.orders = orders);
  }

  delete(id:number){
    this.api.deleteOrder(id).pipe(
      catchError(() => {
        this.loading = false;
        this.snackbar.open('Order failed to delete', null, {
          duration: 2000,
        });
        return throwError(EMPTY);
      })
    ).subscribe(() => {
      this.snackbar.open('Order deleted', null, {
        duration: 2000,
      });
      let orderIdx = this.orders.findIndex(order => order.Id === id);
      if (orderIdx != null) {
        this.orders.splice(orderIdx, 1);
        this.matTable.renderRows();
      }
    });


  }

  edit(id: number) {
    this.router.navigate(['order', id]);
    
  }

}
