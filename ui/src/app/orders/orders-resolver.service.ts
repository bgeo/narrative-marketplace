import { Injectable } from '@angular/core';
import {
  Router, Resolve,
  RouterStateSnapshot,
  ActivatedRouteSnapshot
} from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { Order } from './models';
import { ApiService } from '@app/api.service';
import { take, mergeMap } from 'rxjs/operators';
  
 
@Injectable({
  providedIn: 'root',
})
export class OrdersResolverService implements Resolve<Order> {
  constructor(
    readonly cs: ApiService,
    readonly router: Router
  ) {}
  
  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Order> {
    let id = +route.paramMap.get('id');
 
    return this.cs.getOrder(id).pipe(
      take(1),
      mergeMap(order => {
        if (order) {
          return of(order);
        } else { // id not found
          this.router.navigate(['']);
          return EMPTY;
        }
      })
    );
  }
}
