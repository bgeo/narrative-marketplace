import { Component, OnInit } from '@angular/core';
import { ApiService } from '@app/api.service';
import { map, catchError } from 'rxjs/operators';
import { of, EMPTY, throwError } from 'rxjs';
import { DataPackageType, Order } from '../models';
import { Router, ActivatedRoute } from '@angular/router';
import { MatSnackBar } from '@angular/material';

@Component({
  selector: 'app-orders-edit',
  templateUrl: './orders-edit.component.html',
  styleUrls: ['./orders-edit.component.scss']
})
export class OrdersEditComponent implements OnInit {

  order: Order;
  dataPackageTypes: DataPackageType[];
  loading: boolean = true;
  constructor(
    readonly api: ApiService,
    readonly router: Router,
    readonly route: ActivatedRoute,
    readonly snackbar: MatSnackBar
  ) { }

  ngOnInit() {
    this.order = this.route.snapshot.data.order;
    this.api.getDataPackageTypes().pipe(
      map(dataPackageTypes => {
        this.loading = false;
        return dataPackageTypes;
      }),
      catchError(() => {
        this.loading = false;
        return of([]);
      })
     ).subscribe(dataPackageTypes => 
      this.dataPackageTypes = dataPackageTypes
    );

  }

  /*
    TODO - More useful validation either on client or server. User has no indication
    for why an order will fail.
  */
  submit(order: Order) {
    this.loading = true;
    const order$ = this.order ? this.api.putOrder(order) : this.api.postOrder(order);
    order$.pipe( 
      catchError(() => {
        this.loading = false;
        this.snackbar.open('Order save failed', null, {
          duration: 2000,
        });
        return throwError(EMPTY);
      })
    ).subscribe(() => {
      this.snackbar.open('Order saved', null, {
        duration: 2000,
      });
      // QUESTION - Go to order list or go to the created order?
      this.router.navigateByUrl('');
    });
  }

}
