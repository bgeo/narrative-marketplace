import { Component, OnInit, EventEmitter, Output, Input } from '@angular/core';
import { Order, DataPackageType } from '../models';
import { FormGroup, FormControl } from '@angular/forms';
import { ApiService } from '@app/api.service';
import { map, catchError } from 'rxjs/operators';
import { of } from 'rxjs';

@Component({
  selector: 'app-orders-form',
  templateUrl: './orders-form.component.html',
  styleUrls: ['./orders-form.component.scss']
})
export class OrdersFormComponent implements OnInit {

  orderForm: FormGroup;
  saving: boolean = false;
  @Input() order: Order; 
  @Input() dataPackageTypes: DataPackageType[]; 
  @Output() onSubmit: EventEmitter<Order> = new EventEmitter<Order>();
  constructor(
  ) { }

  ngOnInit() {
    this.createForm();
  }

  createForm() {
    const order = this.order || {} as Order;
    this.orderForm = new FormGroup({
      Name: new FormControl(order.Name),
      MaxBidPrice: new FormControl(order.MaxBidPrice),
      DataPackageTypeId: new FormControl(order.DataPackageTypeId),
    });
  }

  submit() {
    this.saving = true;
    let order = {...this.order} as Order;
    order.Name = this.orderForm.get('Name').value;
    order.MaxBidPrice = this.orderForm.get('MaxBidPrice').value;
    order.DataPackageTypeId = this.orderForm.get('DataPackageTypeId').value;
    this.onSubmit.emit(order);
  }
}
