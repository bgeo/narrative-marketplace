export interface Order {
    Id: number;
    Name: string;
    MaxBidPrice: number;
    DataPackageTypeId: number;
    DataPackageType: string;
}