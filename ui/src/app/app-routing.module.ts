import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { OrdersComponent } from './orders/orders/orders.component';
import { OrdersResolverService } from './orders/orders-resolver.service';
import { MainComponent } from './main/main.component';
import { OrdersEditComponent } from './orders/orders-edit/orders-edit.component';

const routes: Routes = [
    {path: '', component: MainComponent, children: [
      { path: 'order/:id', component: OrdersEditComponent, resolve: {
        order:OrdersResolverService
      }},
      { path: 'order', component: OrdersEditComponent},
      { path: '', component: OrdersComponent }
    ]
    }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {enableTracing:true})],
  providers: [
    OrdersResolverService
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
