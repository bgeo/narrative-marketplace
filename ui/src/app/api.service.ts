import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { throwError, Observable } from 'rxjs';
import { catchError} from 'rxjs/operators';
import { environment } from '@env/environment';
import { Order, DataPackageType } from './orders/models';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  constructor(
    readonly http: HttpClient
  ) { }

  get<T>(path: string): Observable<T> {
    let requestUrl = `${environment.api}/${path}`;
    let headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    return this.http.get<T>(requestUrl, {headers: headers})
      .pipe(
        catchError(this.handleError)
      );
  }

  post<T>(path: string, data:T): Observable<T> {
    let requestUrl = `${environment.api}/${path}`;
    let headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    return this.http.post<T>(requestUrl, data, {headers: headers})
      .pipe(
        catchError(this.handleError)
      );
  }

  put<T>(path: string, data:T): Observable<T> {
    let requestUrl = `${environment.api}/${path}`;
    let headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    return this.http.put<T>(requestUrl, data, {headers: headers})
      .pipe(
        catchError(this.handleError)
      );
  }


  delete(path: string) {
    let requestUrl = `${environment.api}/${path}`;
    let headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    return this.http.delete(requestUrl, {headers: headers})
      .pipe(
        catchError(this.handleError)
      );
  }




  private handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      return throwError('An error occurred:' + error.error.message);
    }
    else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong,
      return throwError(`Backend returned code ${error.status}, ` +
        `error: ${error.error}`);
    }
  };


  getOrders(): Observable<Order[]> {
    return this.get<Order[]>(`Orders`);
  }

  getOrder(id:number): Observable<Order> {
    return this.get<Order>(`Orders/${id}`);
  }



  getDataPackageTypes(): Observable<DataPackageType[]> {
    return this.get<DataPackageType[]>(`DataPackageTypes`);
  }

  postOrder(order: Order): Observable<Order> {
    return this.post<Order>(`Orders`, order);
  }

  putOrder(order: Order): Observable<Order> {
    return this.put<Order>(`Orders/${order.Id}`, order);
  }

  deleteOrder(id: number) {
    return this.delete(`Orders/${id}`);
  }

}