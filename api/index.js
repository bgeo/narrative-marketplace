const http = require('http');
const express = require('express');
const cors = require('cors')
const sqlite3 = require('sqlite3');
const app = express();
const db = new sqlite3.Database('../db/app.db');


app.use(express.json());
app.use(cors()); // TODO - Env Specific CORS

const withStatusCode = (statusCode) => {
  return (res, msg) => {
    res.statusCode = statusCode;
    res.statusMessage = msg;
    return res.json();
  }
}
const applicationError = withStatusCode(500);
const serverError =  withStatusCode(400);

app.get('/api/Orders', (req, res) => {  
    db.all(`
      SELECT o.Id, o.Name, o.MaxBidPrice, o.DataPackageTypeId, d.name AS DataPackageType 
      FROM Orders o
      LEFT JOIN DataPackageTypes d ON d.Id = o.DataPackageTypeId`, (error, row) => {
        if (error) return applicationError(res, error.message);
        res.statusCode = 201;
        return res.json(row);
    })
});

app.get('/api/Orders/:id', (req, res) => {  
  const id = +req.params.id;
  db.get(`
    SELECT o.Id, o.Name, o.MaxBidPrice, o.DataPackageTypeId, d.name AS DataPackageType 
    FROM Orders o
    LEFT JOIN DataPackageTypes d ON d.Id = o.DataPackageTypeId
    WHERE o.Id = $Id`, {$Id: id}, (error, row) => {
      if (error) return applicationError(res, error.message);
      res.statusCode = 201;
      return res.json(row);
  })
});

app.get('/api/DataPackageTypes', (req, res) => {  
  db.all("SELECT Id, Name FROM DataPackageTypes", (error, row) => {
      if (error) return applicationError(res, error.message);
      res.statusCode = 201;
      return res.json(row);
  })
});

const isEmpty = (x) => x == null || x.length == 0;


app.post('/api/Orders', (req, res) => {  
  const { Name , MaxBidPrice , DataPackageTypeId } = req.body
  if (isEmpty(Name) || isEmpty(MaxBidPrice) || isEmpty(DataPackageTypeId)) {
    return serverError(res, "Validation failed.")
  }
  db.run("INSERT INTO Orders (Name, MaxBidPrice, DataPackageTypeId) VALUES ($Name, $MaxBidPrice, $DataPackageTypeId)", {
      $Name: Name,
      $MaxBidPrice: MaxBidPrice,
      $DataPackageTypeId: DataPackageTypeId
  }, function (error) {
    if (error) return serverError(res, error.message);
    res.statusCode = 201;
    return res.json({id:this.lastID}); // last inserted record
  });
});

app.delete('/api/Orders/:id', (req, res) => {  
  const id = +req.params.id;
  db.run("DELETE FROM Orders WHERE Id = $Id", {
      $Id: id,
  }, (error) => {
    if (error) return serverError(res, error.message);
    res.statusCode = 200;
    return res.json({id:this.lastID}); // last inserted record
  });

});

app.put('/api/orders/:id', (req, res) => {  
  const { Id, Name , MaxBidPrice , DataPackageTypeId } = req.body;
  if (isEmpty(Name) || isEmpty(MaxBidPrice) || isEmpty(DataPackageTypeId)) {
    return serverError(res, "Validation failed.")
  }
  db.run(`
    UPDATE Orders SET 
      Name = $Name, 
      MaxBidPrice = $MaxBidPrice, 
      DataPackageTypeId = $DataPackageTypeId
    WHERE
      Id = $Id
    `, {
      $Name: Name,
      $MaxBidPrice: MaxBidPrice,
      $DataPackageTypeId: DataPackageTypeId,
      $Id: Id
  }, (error) => {
    if (error) return serverError(res, error.message);
    res.statusCode = 200;
    return res.json(); // last inserted record
  });

});

app.set('port', 3010);
http.createServer(app).listen(app.get('port'), "127.0.0.1");
