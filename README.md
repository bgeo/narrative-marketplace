# narrative-marketplace

An example project for an order system. Supports basic CRUD operations for viewing, creating, editing, and deleting orders.

Meets the basic functionality but no one part of the system is robust in any particular way.

## Overview

Home Page displays a list of orders:

![Orders](/images/Orders.png "Orders")

Clicking New Order at the top right opens an order form:

![Order Form](/images/OrderForm.png "Order Form")

On submission, creating a form will navigate back to the orders page. From there the menu icon in each row of the table will open up edit and delete tools.

![Order Actions](/images/OrderActions.png "Order Actions")

Editing will go back to the order form with the fields filled out with the current order:

![Order Edit](/images/OrderEdit.png "Order Edit")

Successfully editing will navigate back to the orders view page.

If deleting an order, the order will be removed from the table and a snack bar notification will indicate the order was deleted:

![Order Delete](/images/OrderDelete.png "Order Delete")

## Tech Stack

The stack was chosen for rapid prototyping and may not meet requirements. The major deficiency is on the database side in choosing SQLite. SQLite is excellent for getting a database spun up quickly (just a file) and can handle heavy read only loads, but is not designed for multi-user editing. 


* SQLite3
* Node.js with Express
* Angular 7 with Angular Material


## Local Deployment

Requirements:

* SQLite3
* Node.js and npm
* Angular CLI installed 


### Database Setup

Run the either one of the two build steps:

Option A:
```
cd db
sqlite3 app.db < build.sql
```

Option B:
```
cd db
chmod +x build.sh
./build.sh
```


This will create an app.db file in the db folder and seeded with three orders.

*NOTE* - Running the build will wipe out an existing database in ~db/app.db

### Server Setup

Run the following:
```
cd api
npm install
npm start
```
The server will be running on http://localhost:3010

### UI Setup

Run the following:
```
cd ui
npm install
npm start
```
The UI will now be running on http://localhost:4200
